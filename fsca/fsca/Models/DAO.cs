﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Web.Configuration;
using System.Security.Cryptography;
using System.Text;

namespace fsca.Models
{
    public class DAO
    {
        SqlConnection conn;
        public string message = "";
        //instantiated sha1 encyption object
        SHA1 sha = SHA1.Create();

        public void Connection()
        {
            string fscaConn = WebConfigurationManager.ConnectionStrings["conString"].ConnectionString;
            conn = new SqlConnection(fscaConn);
        }

        public int Insert(Accounts account)
        {
            //taking account.Password from Create Account input, hashing/encrypting it and storing the encryption in database
            byte[] hashData = sha.ComputeHash(Encoding.Default.GetBytes(account.Password));
            
            StringBuilder sb = new StringBuilder();

            for (int i = 0; i < hashData.Length; i++)
            {
                sb.Append(hashData[i].ToString());
            }

            int count = 0;

            Connection();

            SqlCommand cmd = new SqlCommand("uspInsertAccount", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.AddWithValue("@first", account.FirstName);
            cmd.Parameters.AddWithValue("@last", account.LastName);
            cmd.Parameters.AddWithValue("@email", account.Email);
            cmd.Parameters.AddWithValue("@dob", account.DOB);
            cmd.Parameters.AddWithValue("@pass", sb.ToString());
            cmd.Parameters.AddWithValue("@sub", account.Subscription);

            try
            {
                conn.Open();
                count = cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            finally
            {
                conn.Close();
            }

            return count;
        }

        public string CheckLogin(Accounts account)
        {
            //taking account.Password from Login input, hashing/encrypting it and comparing the encryption with stored password in database
            byte[] hashData = sha.ComputeHash(Encoding.Default.GetBytes(account.Password));

            StringBuilder sb = new StringBuilder();

            for (int i = 0; i < hashData.Length; i++)
            {
                sb.Append(hashData[i].ToString());
            }

            Connection();
            SqlCommand cmd = new SqlCommand("CheckLogin", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.AddWithValue("@email", account.Email);
            cmd.Parameters.AddWithValue("@pass", sb.ToString());
            try
            {
                conn.Open();
                account.FirstName = (string)cmd.ExecuteScalar();
                
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            finally
            {
                conn.Close();
            }

            return account.FirstName;
        }
    }
}
