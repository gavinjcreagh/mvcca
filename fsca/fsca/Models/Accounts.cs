﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace fsca.Models
{
    public class Accounts
    {
        [Display(Name = "First Name:")]
        [Required(ErrorMessage = "Please enter your First Name.")]
        [RegularExpression("^[A-Za-z]+$", ErrorMessage = "Only letters are allowed.")]
        public string FirstName { get; set; }

        [Display(Name = "Last Name:")]
        [Required(ErrorMessage = "Please enter your Last Name.")]
        [RegularExpression("^[A-Za-z]+$", ErrorMessage = "Only letters are allowed.")]
        public string LastName { get; set; }

        [Display(Name = "Email:")]
        [Required(ErrorMessage = "Please enter a valid Email Address.")]
        [RegularExpression(@"[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?", ErrorMessage = "Invalid Email Address")]
        public string Email { get; set; }

        [Display(Name = "Date of Birth")]
        [Required(ErrorMessage = "Please enter your Date of Birth (dd/mm/yyyy).")]
        [DataType(DataType.Date), DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        [RegularExpression(@"^(?:(?:31(\/|-|\.)(?:0?[13578]|1[02]))\1|(?:(?:29|30)(\/|-|\.)(?:0?[1,3-9]|1[0-2])\2))(?:(?:1[6-9]|[2-9]\d)?\d{2})$|^(?:29(\/|-|\.)0?2\3(?:(?:(?:1[6-9]|[2-9]\d)?(?:0[48]|[2468][048]|[13579][26])|(?:(?:16|[2468][048]|[3579][26])00))))$|^(?:0?[1-9]|1\d|2[0-8])(\/|-|\.)(?:(?:0?[1-9])|(?:1[0-2]))\4(?:(?:1[6-9]|[2-9]\d)?\d{2})$", ErrorMessage = "Please enter a valid date (dd/mm/yyyy).")]
        public DateTime DOB { get; set; }

        [Display(Name = "Password:")]
        [DataType(DataType.Password)]
        [Required(ErrorMessage = "Please enter a valid Password.")]
        [StringLength(10, ErrorMessage = "Password must contain 6-10 characters long", MinimumLength = 6)]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirm Password:")]
        [Required(ErrorMessage = "Please enter a valid Password.")]
        [StringLength(10, ErrorMessage = "Password must contain 6-10 characters long", MinimumLength = 6)]
        [Compare("Password", ErrorMessage = "Passwords did not match.")]
        public string ConfirmPassword { get; set; }

        public bool Subscription { get; set; }

    }
}