﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using fsca.Models;


namespace fsca.Controllers
{
    public class AccountController : Controller
    {
        // GET: Account
       
        [HttpGet]
        public ActionResult CreateAcc()
        {
            return View();
        }

        [HttpPost]
        public ActionResult CreateAcc(Accounts account)
        {
            DAO dataAccessObj = new DAO();

            if (ModelState.IsValid)
            {
                int count = dataAccessObj.Insert(account);

                if (count > 0)
                {
                    ViewData["message"] = "Account Successfully Created.";
                }

                else
                {
                    ViewData["message"] = "Error: " + dataAccessObj.message;
                }

                return View("Message");
            }

            else
            {
                return View("Register");
            }
            
        }

        public ActionResult MyAccount()
        {
            return View();
        }

        public ActionResult ShoppingCart()
        {
            return View();
        }


    }
}