﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace fsca.Controllers
{
    public class PaymentController : Controller
    {
        // GET: Payment
        public ActionResult Checkout()
        {
            return View();
        }

        public ActionResult Payment()
        {
            return View();
        }

        public ActionResult PaymentSuccess()
        {
            return View();
        }

        public ActionResult PaymentFailure()
        {
            return View();
        }
    }
}