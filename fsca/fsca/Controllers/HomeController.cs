﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using fsca.Models;

namespace fsca.Controllers
{
    public class HomeController : Controller
    {
        // GET: Home
        public ActionResult Products()
        {
            return View();
        }
        [HttpGet]
        public ActionResult Login()
        {
            return View();
        }
        [HttpPost]
        public ActionResult Login(LoginModel login)
        {
            DAO dataAccessObj = new DAO();
            Accounts account = new Accounts();

            if (ModelState.IsValid)
            {
                account.Email = login.Email;
                account.Password = login.Password;
                account.FirstName = dataAccessObj.CheckLogin(account);

                Session.Add("accountName", account.FirstName);

                if (Session["accountName"] == null)
                {
                    ViewData["message"] = "Error: " + dataAccessObj.message;
                    return View("Message");
                }
                else
                {
                    return RedirectToAction("Index");
                }
            }
            else
            {
                return View("Login");
            }
        }
        public ActionResult Logout()
        {
            Session.Clear();

            return RedirectToAction("Index");
        }
    }
}